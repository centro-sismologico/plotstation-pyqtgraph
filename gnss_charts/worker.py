import sys
import os
import asyncio
import dataclasses
import time 
import signal
import time
import operator
from functools import reduce
from pathlib import Path
from enum import IntEnum, auto
from datetime import datetime, timedelta

from dataclasses import dataclass as std_dataclass, asdict
from typing import Dict, Any, List, Tuple, Callable, Union
from pydantic.dataclasses import dataclass
from pydantic import ValidationError, validator
import copy

from PySide6.QtCore import (
    QObject,
    QRunnable,
    QThreadPool,
    QTimer,
    Signal,
    Slot)
from PySide6.QtCore import QEventLoop

import numpy as np
from rethinkdb import RethinkDB

from networktools.messages import MSGException, MessageLog
from networktools.ip import validURL, validPORT
from networktools.library import geojson2json
from networktools.time import get_datetime_di
from networktools.time import timestamp, now
from data_rdb import Rethink_DBS
from tasktools.taskloop import TaskLoop

from rich import print
from dacite import from_dict
from networktools.geo import GeoData, TimeData, NEUData, DataPoint
from .read_data import read_csv, Mode, load_stations

ORM_URL = os.getenv("ORM_SERVICE_HOST",'http://10.54.218.196')

rdb = RethinkDB()


def signal_handle(_signal, frame):
    print("Stopping the Jobs.")
    raise Exception("Breaking the jobs with keyboard")

signal.signal(signal.SIGINT, signal_handle)

class KeyboardInterruptError(Exception): pass

class DBStep(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    COLLECT = auto()

class DBSend(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    SEND = auto()


Dataset = List[Dict[str,Any]]

@dataclass
class DBData:
    host: str
    port: int
    dbname: str

    @validator("host")
    def check_host_is_url(cls, value):
        assert validURL(value), "It's not a valid host url"
        return value
    
    @validator("port")
    def check_port_is_valid(cls, value):
        assert validPORT(value), "It's not a valid port value 0 to 65534"
        return value

    def dict(self):
        return asdict(self)


class Action(IntEnum):
    CREATE = auto()
    UPDATE = auto()
    LOG =  auto()
    DROP = auto()

@dataclass
class DataGNSS:
    status: Dict[str,Any]
    data: GeoData

    def dict(self):
        return asdict(self)

@dataclass(frozen=True)
class StationAction:
    action: Action
    data: Union[DataGNSS,Dict[str,Any]]

    def dict(self):
        return asdict(self)



@std_dataclass
class WorkerSignals(QObject):
    """
    Signals to control the messages
    """
    finished = Signal()
    log = Signal(MessageLog)
    error = Signal(MSGException)
    result = Signal(StationAction)
    station = Signal(StationAction)
    default = Signal(StationAction) 
    
    def __post_init__(self):
        super().__init__()

    def dict(self):
        return asdict(self)

class Worker(QRunnable, QObject):
    def __init__(self, queue_reader,  window, *args, **kwargs):
        super().__init__()
        self.queue_reader = queue_reader
        self.window = window
        self.signals = WorkerSignals()


    @Slot()
    def run(self):
        try:
            #for _ in range(self.queue_reader.qsize()):
            for data in self.queue_reader.get():
            #data = self.queue_reader.get()
                if data.action == Action.CREATE:
                    self.signals.station.emit(data)
                elif data.action == Action.UPDATE:
                    self.signals.result.emit(data)
                else:
                    self.signals.default.emit(data)

            # update the frame with all new data
            self.window.get_frame()
        except Exception as e:
            raise e
        except KeyBoardInterrupt:
            raise KeyboardInterruptError()


def geomean(geodataset:List[GeoData]):
    """
    sum([GeoData])
    """
    new = [g for g in geodataset if g]
    if new:
        g0 = geodataset[0]
        for i,g in enumerate(geodataset):
            if i>0:
                g0 += g           
        return g0



class WorkerReader:
    """
    Worker for qt app
    obtain the data and send
    """
    def __init__(self, 
                 dbdata:DBData, 
                 queue_write, 
                 api_url,
                 delta_time=2,
                 *args, 
                 **kwargs):
        super().__init__()
        self.api_url = api_url
        self.dbdata = dbdata
        self.queue_write = queue_write
        self.args = args
        self.kwargs = kwargs
        self.delta_time  = 5
        if kwargs.get("update_stations"):
            self.stations = self.load_stations(self.queue_write,
                                               api_url, send=False)
        di = rdb.iso8601(get_datetime_di(delta=self.delta_time))
        self.set_state(DBStep.CREATE, None, self.stations, di)
        self.time_update = timedelta(seconds=300)
        self.next_update = datetime.utcnow() + self.time_update

    def get_state(self) -> Tuple[Tuple[DBStep, Rethink_DBS, Dataset, datetime], Dict[str,Any]]:
        return (
            self.control,
            self.db_insta,
            self.dataset,
            self.di
        ), self.kwargs

    @classmethod
    def load_stations(cls, queue, api_url, send=True):
        # keys = {"id", "code", "name", "host", "interface_port",
        #         "protocol_host", "port", "ECEF_X", "ECEF_Y", "ECEF_Z",
        #         "db", "protocol", "active", "server_id", "network_id",
        #         "table_name", "POSITION"}
        # filename = Path(__file__).parent / "csv/station.csv"
        stations = load_stations(api_url)#read_csv(filename, Mode.GSOF)
        if send:
            for code, station in stations:
                station_copy = {key:value for key,value in
                                station.items() if key != 'process_instance'}
                new_action = StationAction(Action.CREATE, station_copy)
                queue.put(new_action)        
        dataset = {code:station for code, station in stations}
        cls.stations = dataset
        return dataset

    def set_state(
            self,
            control: DBStep,
            db_insta: Rethink_DBS,
            dataset: Dataset,
            di:datetime, **kwargs):
        self.control = control
        self.db_insta = db_insta
        self.dataset = dataset
        self.di = di
        self.kwargs = kwargs


    async def obtain_data(self, control, db_insta, dataset, di, **kwargs):
        # do work
        now = datetime.utcnow()
        if now >= self.next_update :
            try:
                new_dataset = self.load_stations(None, self.api_url, send=False)
                self.next_update = now + self.time_update
                for k, item in dataset.items():
                    item["process_instance"].close()
                dataset = new_dataset
            except Exception as e:
                print(now(), f"Un error al actualizar stations >{e}<")

        delta_time = int(os.environ.get("DELTA_TIME", 10))
        if control == DBStep.CREATE:
            opts = self.dbdata.dict()
            db_insta = Rethink_DBS(**opts)
            control = DBStep.CONNECT
        elif control == DBStep.CONNECT:
            fail = False
            dbname = self.dbdata.dbname
            try:
                await asyncio.wait_for(db_insta.async_connect(),timeout=10)
                await db_insta.list_dbs()
                await db_insta.list_tables()
            except asyncio.TimeoutError as e:
                print(f"Falla al intentar conectar, {e}")
                control = DBStep.CONNECT
                fail = True
                await asyncio.sleep(30)
            if not fail:
                control = DBStep.COLLECT
        if control == DBStep.COLLECT:
            key = "DT_GEN"
            filter_opt = {'left_bound': 'open', 'index': key}
            df = rdb.iso8601(get_datetime_di(delta=0))
            # listar las tablas
            await db_insta.list_tables()
            try:
                for code, station in dataset.items():
                    table_name = station.get("table_name")
                    delta_dt = [di, df]
                    cursor = await db_insta.get_data_filter(
                            table_name,
                            [di, df],
                            filter_opt,
                            key)
                    geodataset = []
                    mean = []
                    recv = []
                    for data in cursor:
                        process_instance = station.get("process_instance")
                        try:
                            if process_instance:
                                if "POSITION_VCV" in data:
                                    geodata = process_instance.manage_data(data)
                                    geojson = geojson2json(geodata,
                                                          destiny='db')
                                    geojson["time"] = {
                                       "recv": data.get("DT_RECV"),
                                       "delta": data.get("DELTA_TIME")
                                    }

                                    recv.append(data.get("DT_RECV"))
                                    mean.append(data.get("DELTA_TIME"))
                                    geodataset.append(
                                        from_dict(
                                            data_class=GeoData,
                                            data=geojson
                                        )
                                    )
                                else:
                                    now = datetime.utcnow()
                                    recv.append(data.get("DT_RECV"))
                                    mean.append(data.get("DELTA_TIME"))
                            else:
                                await asyncio.sleep(2.5)
                        except Exception as e:
                            print("Data error", data)
                            await asyncio.sleep(2.5)
                            raise e
                    if mean and recv:
                        array = np.array(mean)
                        now = datetime.utcnow()
                        start = recv[0]
                        end = recv[-1]
                        delta = np.mean(array)
                        std = np.std(mean)
                        info = {
                            "start": start.isoformat(),
                            "end": end.isoformat(),
                            "delta_time":delta,
                            "std": std
                        }
                        # emit data to GUI
                        geodata = geomean(geodataset)
                        msg = DataGNSS(info, geodata)
                        new_action = StationAction(Action.UPDATE, msg)
                        self.queue_write.put(new_action)
            except asyncio.TimeoutError as te:
                print(f"Exception {te}")
                control = DBStep.CONNECT
            di = df
        if control == DBStep.COLLECT:
            await asyncio.sleep(delta_time)

        # set state
        return (control, db_insta, dataset, di), kwargs

    def task(self):
        loop = asyncio.get_event_loop()
        #self.load_stations(self.queue_write)
        (control, db_insta, dataset, di), kwargs  = self.get_state()
        args = [control, db_insta, dataset, di]
        task = TaskLoop(
            self.obtain_data, 
            args, 
            {},
            task_name="obtain_data")
        task.create()
        loop.run_forever()
        


