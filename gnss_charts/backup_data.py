from .queue_set import QueueSet
import math
import functools
import yaml
import concurrent.futures
from multiprocessing import Manager
import typer
import sys
import os
import asyncio
import dataclasses
import time
import signal
import time
import operator
from functools import reduce
from pathlib import Path
from enum import IntEnum, auto
from datetime import datetime, timedelta

from dataclasses import dataclass as std_dataclass, asdict
from typing import Dict, Any, List, Tuple, Callable, Union
from pydantic.dataclasses import dataclass
from pydantic import ValidationError, validator
import copy
import ujson as json
from PySide6.QtCore import (
    QObject,
    QRunnable,
    QThreadPool,
    QTimer,
    Signal,
    Slot)
from PySide6.QtCore import QEventLoop
import pytz
import numpy as np
from rethinkdb import RethinkDB

from networktools.messages import MSGException, MessageLog
from networktools.ip import validURL, validPORT
from networktools.library import geojson2json
from networktools.time import get_datetime_di
from networktools.time import timestamp, now
from data_rdb import Rethink_DBS
from tasktools.taskloop import TaskLoop

from rich import print
from dacite import from_dict
from networktools.geo import GeoData, TimeData, NEUData, DataPoint
from .read_data import read_csv, Mode, load_stations

ORM_URL = os.getenv("ORM_SERVICE_HOST", 'http://10.54.218.196')

rdb = RethinkDB()


def signal_handle(_signal, frame):
    print("Stopping the Jobs.")
    raise Exception("Breaking the jobs with keyboard")


signal.signal(signal.SIGINT, signal_handle)


class KeyboardInterruptError(Exception):
    pass


class DBStep(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    COLLECT = auto()


class DBSend(IntEnum):
    CREATE = auto()
    CONNECT = auto()
    SEND = auto()


Dataset = List[Dict[str, Any]]


@dataclass
class DBData:
    host: str
    port: int
    dbname: str

    @validator("host")
    def check_host_is_url(cls, value):
        assert validURL(value), "It's not a valid host url"
        return value

    @validator("port")
    def check_port_is_valid(cls, value):
        assert validPORT(value), "It's not a valid port value 0 to 65534"
        return value

    def dict(self):
        return asdict(self)


class Action(IntEnum):
    CREATE = auto()
    UPDATE = auto()
    LOG = auto()
    DROP = auto()


@dataclass
class DataGNSS:
    status: Dict[str, Any]
    data: GeoData

    def dict(self):
        return asdict(self)


@dataclass(frozen=True)
class StationAction:
    action: Action
    data: Union[DataGNSS, Dict[str, Any]]

    def dict(self):
        return asdict(self)


def geomean(geodataset: List[GeoData]):
    """
    sum([GeoData])
    """
    new = [g for g in geodataset if g]
    if new:
        g0 = geodataset[0]
        for i, g in enumerate(geodataset):
            if i > 0:
                g0 += g
        return g0


def create(base, code):
    print("Creating file", code)
    path = base / f"{code}.json"
    writer = path.open("w")
    writer.write("[\n")
    return writer


class Worker:
    end_time: datetime
    base: Path = Path("DATA_BACKUP")

    def __init__(self, queue_reader, end_time, *args, **kwargs):
        super().__init__()
        self.end_time = end_time
        self.base.mkdir(parents=True, exist_ok=True)
        self.queue_reader = queue_reader

    async def async_run(self, writer_dict, **kwargs):
        # for _ in range(self.queue_reader.qsize()):
        if not self.queue_reader.empty():
            for i in range(self.queue_reader.qsize()):
                action, dataset = self.queue_reader.get()
                if action == Action.CREATE:
                    print("Action-msg CREATE", action)
                elif action == Action.UPDATE:
                    if dataset:
                        for item in dataset:
                            code = item.station
                            writer = writer_dict.get(
                                code)
                            if writer != "CLOSED":
                                if not writer:
                                    writer = create(self.base, code)
                                    writer_dict[code] = writer
                                data = json.dumps(item.dict(),
                                                  default=str)
                                if item.dt_gen >= self.end_time:
                                    print("Closing")
                                    writer.write(data+"\n]")
                                    writer.close()
                                    writer_dict[code] = "CLOSED"
                                    print(f"Closing json {code}")
                                else:
                                    writer.write(data+",\n")

            # if action == Action.UPDATE:
            #     print("Action", action)
            #     for data in msg.data:
            #         print("On worker", data)
            # update the frame with all new data
        return [writer_dict], {}

    def run(self):
        loop = asyncio.get_event_loop()
        # self.load_stations(self.queue_write)
        writer_dict = {}
        args = [writer_dict]
        task = TaskLoop(
            self.async_run,
            args,
            {},
            task_name="write_data")
        task.create()
        loop.run_forever()


class WorkerReader:
    """
    Worker for qt app
    obtain the data and send
    """

    def __init__(self,
                 dbdata: DBData,
                 queue_write,
                 api_url,
                 delta_time=2,
                 *args,
                 **kwargs):
        super().__init__()
        self.api_url = api_url
        self.dbdata = dbdata
        self.queue_write = queue_write
        self.args = args
        self.kwargs = kwargs
        self.delta_time = 5
        if kwargs.get("update_stations"):
            self.stations = self.load_stations(self.queue_write,
                                               api_url, send=False)
        di = rdb.iso8601(get_datetime_di(delta=self.delta_time))
        self.set_state(DBStep.CREATE, None, self.stations, di)
        self.time_update = timedelta(seconds=300)
        self.next_update = datetime.utcnow() + self.time_update

    def get_state(self) -> Tuple[Tuple[DBStep, Rethink_DBS, Dataset, datetime], Dict[str, Any]]:
        return (
            self.control,
            self.db_insta,
            self.dataset,
            self.di
        ), self.kwargs

    @ classmethod
    def load_stations(cls, queue, api_url, send=True):
        # keys = {"id", "code", "name", "host", "interface_port",
        #         "protocol_host", "port", "ECEF_X", "ECEF_Y", "ECEF_Z",
        #         "db", "protocol", "active", "server_id", "network_id",
        #         "table_name", "POSITION"}
        # filename = Path(__file__).parent / "csv/station.csv"
        stations = load_stations(api_url)  # read_csv(filename, Mode.GSOF)
        if send:
            for code, station in stations:
                station_copy = {key: value for key, value in
                                station.items() if key != 'process_instance'}
                new_action = (Action.CREATE, station_copy)
                queue.put(new_action)
        dataset = {code: station for code, station in stations}
        cls.stations = dataset
        return dataset

    def set_state(
            self,
            control: DBStep,
            db_insta: Rethink_DBS,
            dataset: Dataset,
            di: datetime, **kwargs):
        self.control = control
        self.db_insta = db_insta
        self.dataset = dataset
        self.di = di
        self.kwargs = kwargs

    async def obtain_data(self, control, db_insta, dataset, di, **kwargs):
        # do work
        now = datetime.utcnow()
        if now >= self.next_update:
            try:
                new_dataset = self.load_stations(
                    None, self.api_url, send=False)
                self.next_update = now + self.time_update
                for k, item in dataset.items():
                    item["process_instance"].close()
                dataset = new_dataset
            except Exception as e:
                print(now(), f"Un error al actualizar stations >{e}<")

        delta_time = int(os.environ.get("DELTA_TIME", 10))
        if control == DBStep.CREATE:
            opts = self.dbdata.dict()
            db_insta = Rethink_DBS(**opts)
            control = DBStep.CONNECT
        elif control == DBStep.CONNECT:
            fail = False
            dbname = self.dbdata.dbname
            try:
                await asyncio.wait_for(db_insta.async_connect(), timeout=10)
                await db_insta.list_dbs()
                await db_insta.list_tables()
            except asyncio.TimeoutError as e:
                print(f"Falla al intentar conectar, {e}")
                control = DBStep.CONNECT
                fail = True
                await asyncio.sleep(30)
            if not fail:
                control = DBStep.COLLECT
        if control == DBStep.COLLECT:
            key = "DT_GEN"
            filter_opt = {'left_bound': 'open', 'index': key}
            df = rdb.iso8601(get_datetime_di(delta=0))
            # listar las tablas
            await db_insta.list_tables()
            try:
                for code, station in dataset.items():
                    table_name = station.get("table_name")
                    delta_dt = [di, df]
                    cursor = await db_insta.get_data_filter(
                        table_name,
                        [di, df],
                        filter_opt,
                        key)
                    geodataset = []
                    for data in cursor:
                        process_instance = station.get("process_instance")
                        try:
                            if process_instance:
                                if "POSITION_VCV" in data:
                                    geodata = process_instance.manage_data(
                                        data)
                                    geojson = geojson2json(geodata,
                                                           destiny='db')
                                    geojson["time"] = {
                                        "recv": data.get("DT_RECV"),
                                        "delta": data.get("DELTA_TIME")
                                    }

                                    geodataset.append(
                                        from_dict(
                                            data_class=GeoData,
                                            data=geojson
                                        )
                                    )
                                else:
                                    now = datetime.utcnow()
                            else:
                                await asyncio.sleep(2.5)
                        except Exception as e:
                            print("Data error", data)
                            await asyncio.sleep(2.5)
                            raise e
                    # emit data to GUI
                    info = {
                        "start": di,
                        "end": df,
                    }
                    # msg = DataGNSS(info, geodataset)
                    new_action = (Action.UPDATE, geodataset)
                    self.queue_write.put(new_action)
            except asyncio.TimeoutError as te:
                print(f"Exception {te}")
                control = DBStep.CONNECT
            di = df
        if control == DBStep.COLLECT:
            await asyncio.sleep(delta_time)

        # set state
        return (control, db_insta, dataset, di), kwargs

    def task(self):
        loop = asyncio.get_event_loop()
        # self.load_stations(self.queue_write)
        (control, db_insta, dataset, di), kwargs = self.get_state()
        args = [control, db_insta, dataset, di]
        task = TaskLoop(
            self.obtain_data,
            args,
            {},
            task_name="obtain_data")
        task.create()
        loop.run_forever()


app = typer.Typer()


def generate_queue_set(mp_queue, manager, groups):
    queue_set = QueueSet(mp_queue, groups)
    for g in groups:
        m_queue = manager.Queue()
        queue_set.add(g, m_queue)
    queue_set.distribute()
    return queue_set


def generate_groups(
        dataset: Dict[str, Any],
        elements: int) -> Dict[str, str]:
    n = max(math.ceil(len(dataset) / elements), 1)
    groups = np.array_split(list(dataset), n)
    return {str(i): g for i, g in enumerate(groups)}


def create_reader(dbdata, queue_data, api_url):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    db_reader = WorkerReader(dbdata, queue_data, api_url)
    db_reader.task()


def run_control(queue_data, save_time):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    endtime = pytz.utc.localize(
        datetime.utcnow() + timedelta(minutes=save_time))
    db_reader = Worker(queue_data, endtime)
    db_reader.run()


def cancel(tasks):
    for t in tasks:
        t.cancel()
        t.result()


@ app.command("backup_data")
def run_backup(settings: Path):
    workers = 2
    if not isinstance(settings, Path):
        print("SETTINGS", settings.__dict__, type(settings))
        settings = Path(settings)
    conf = settings.read_text()
    conf_data = yaml.safe_load(conf)

    with concurrent.futures.ProcessPoolExecutor(workers) as executor:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        manager = Manager()
        mp_queue = manager.Queue()
        dataset = WorkerReader.load_stations(
            mp_queue, conf_data["source"]["url"])

        tasks = []
        groups = generate_groups(dataset, elements=100)
        dbopts = conf_data.get("dbdata", {})

        dbdata = None
        if dbopts:
            dbdata = DBData(**dbopts)
        else:
            print("No db data on settings.yaml")

        task = loop.run_in_executor(
            executor,
            functools.partial(
                create_reader,
                dbdata,
                mp_queue,
                conf_data["source"]["url"]))
        tasks.append(task)
        save_time = conf_data["save_time"]
        task = loop.run_in_executor(
            executor,
            functools.partial(
                run_control,
                mp_queue, save_time))
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            cancel(tasks)
            raise e
        except KeyboardInterrupt as ki:
            cancel(tasks)
            raise ki


def main():
    app()


if __name__ == "__main__":
    app()
