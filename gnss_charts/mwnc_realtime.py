# -*- coding: utf-8 -*-
import asyncio
import concurrent.futures
import functools
import multiprocessing as mp
import queue
import random
import sys
import math
import click
import yaml
import typer

from functools import partial
from multiprocessing import Manager, Queue
from datetime import datetime
from typing import List, Any, Type, Dict, TypeVar

import numpy as np
from rich import print

# PYSIDE QT-SIDE:
import pyqtgraph as pg

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QListWidget,
    QScrollArea
)
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot,
    Qt
)
from PySide6.QtGui import QFont,QGuiApplication

from qasync import QEventLoop, QThreadExecutor

# own modules::::
from .data import Station
from .control_group import ControlWindow, ChartAction
from .join_rb_plot import ItemDataJoinRingBufferPlot
from .multi_station_layout_widget import MultiStationLayoutWidget
from .plot_station import PlotStation
from .queue_set import QueueSet
from .station_ring_buffer import StandardRingBuffer 
from .station_layout_widget import StationLayoutWidget
from .utils import TimeAxisItem, timestamp
from .worker import (
    Worker,
    DBData,
    WorkerReader,
    Action,
    WorkerSignals
)

T = TypeVar('T')

AEventLoop = type(asyncio.get_event_loop())

class QEventLoopPlus(QEventLoop, AEventLoop):
    pass

def create_ring_buffer(seconds):
    data = dict(
        seconds=seconds,
        name="test",
        time_field="dt_gen",
        ringbuffer=[],
        keys=["N","E","U"]
    )
    rb = StandardRingBuffer(**data)
    return rb

class SimpleMultiplotLayoutWidget(pg.GraphicsLayoutWidget):
    joins:List[ItemDataJoinRingBufferPlot] = []

    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()

        """
        Aqui se lee la cola desde worker -> obtain_data 
        -> se recibe: StationAction -> (action, DataGNSS) ->  (info, geodata:DataItem)
        """

        # leer de cola el dato 'StationAction'
        # extraer  msg, DataGNSS
        # enviar DataItem a cada parte, cada JOINRB corresponde a una
        # estacion.
        # acumular en una lista los datiems correspondientes a cada jb
        try:
            for join in self.joins:
                join.update()
        except Exception as e:
            print("Error update chart", datetime.utcnow(), e)
            raise e
    # def set_update(self, update):
    #     self.update = update

    def add_join(self, joinrb):
        self.joins.append(joinrb) 




class MainWindow(QMainWindow):
    groups:Dict[str, MultiStationLayoutWidget]
    stations:Dict[str, Station]

    def __init__(self, names=[], stations={}, geom=None, queue=queue):
        super().__init__()
        if len(names)==0:
            names.append("MAIN")
        self.stations = stations
        # queue  lista de comunicación
        self.queue = queue

       # parent_layout = QVBoxLayout()
       # w = QWidget()
        main_layout = SimpleMultiplotLayoutWidget(size=(500, 100))
        print("Enabling scroll bar")
        self.scroll = QScrollArea()
        #scroll_area.setAlignment(Qt.AlignRight)
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(main_layout)
        
        #main_layout.useOpenGL()

        self.jrbp = self.create_station_element(main_layout, names)
        #win.start(60)
        #self.main_win = self.create_charts(w, groups)
        self.main_win = main_layout
        #parent_layout.addWidget(self.scroll)
        #w.setLayout(parent_layout)
        self.setCentralWidget(self.scroll) # antes era: w
        # the timer could activate on the active window?
        self.threadpool = QThreadPool()
        self.set_timer(self.main_win)
        # self.check_commands()
        # neew activate with a button
        self.showMaximized()
        #self.control_window(self.jrbp)

    def control_window(
            self,
            groups):
        control = ControlWindow(self, groups)
        control.show()

    def create_station_element(
            self,
            main_layout, 
            names):
        #names = [f"STA{i:02d}" for i in range(45)]
        jrbp_dict = {g:{} for g in names}
        plot_before = None
        len_g = len(names)
        height_base = 150
        height_top = 30
        height_bottom = 30
        for position, name in enumerate(names):
            station_layout = StationLayoutWidget()
            station_layout.set_elements(len_g)
            station_layout.set_time(300)
            station_layout.setContentsMargins(0, 0, 0, 0)
            title_names = False
            axis_time = False
            if position == 0:
                title_names =  True
            elif position == len(names)-1:
                axis_time = True
            plot_station = station_layout.create_chart(
                name,
                title_names,
                axis_time,
                main_layout,
                plot_before)#create_chart(pg, win)
            # adding item to main layout
            plot_before = plot_station
            main_layout.addItem(station_layout)
            # lalala
            ring_buffer = create_ring_buffer(plot_station.seconds)

            height = 0
            if position == 0:
                height += height_top
            elif position== len(names)-1:
                height += height_bottom

            jrbp = ItemDataJoinRingBufferPlot(
                name,
                position,
                ring_buffer,
                station_layout, 
                extra_height=height)
            jrbp_dict[name] = jrbp
            main_layout.add_join(jrbp)
            main_layout.addItem(station_layout, row=position,
                                col=0)
        # load only 1 group
        #jrbp.create_chart()
        return {"group1":jrbp_dict}


    def create_charts(self, parent, groups):
        """
        TODO: Check if parent is for all...
        """
        print("Creating charts")
        for g in groups:
            stations_group = {code:station for code, station in
                              self.stations.items() if station.group==g}
            win = MultiStationLayoutWidget(size=(500,100)).start(
                g,
                stations_group)
            self.groups[g] = win
        main_win = self.groups[groups[0]]
        pg.setConfigOptions(antialias=True)
        return main_win

   
    def set_timer(self, win):
        self.timer= QTimer(self)
        self.timer.timeout.connect(partial(self.thread_read_data, win))
        self.timer.start(2000)

    def progress_fn(self, n):
        print(f"{n} done")

    def print_output(self, s):
        """
        Receive data and put in ringbuffer
        """
        group = self.jrbp["group1"]
        data = s.data.data
        name = data.station
        joinbuffer = group.get(name)
        if joinbuffer:
            joinbuffer.add(data)


    def print_station(self, s):
        print(s)

    def print_log(self, s):
        print("Log", id(s))

    def print_error(self, s):
        print(s)

    def thread_complete(self):
        print("Thread complete")

    def update_data(self):
        pass

    def thread_read_data(self, win):
        try:
            # replace dummyworker with worker
            # worker_data = DummyWorker(self.queue, win, [], {})
            worker_data = Worker(self.queue,  win, [], {})
            
            # UPDATE STATION
            worker_data.signals.result.connect(
                self.print_output)
            worker_data.signals.station.connect(
                self.print_station)
            worker_data.signals.log.connect(self.print_log)
            worker_data.signals.error.connect(
                self.print_error)
            worker_data.signals.finished.connect(
                self.thread_complete)
            worker_data.signals.default.connect(
                self.print_log)

            #self.threadpool.start(win.get_frame)
            self.threadpool.start(worker_data)
            win.update()
            self.update()
        except Exception as e:
            raise e

    def update(self):
        for jrbp in self.jrbp["group1"].values():
            jrbp.update()


    def check_commands(self):
        self.timer= QTimer(self)
        self.timer.timeout.connect(self.read_queue)
        self.timer.start(1000)

    def read_queue(self):
        try:
            if self.queue.qsize() > 0:
                for i in range(self.queue.qsize()):
                    elem = self.queue.get()
                    self.queue.task_done()
                print("Queue task done...")
        except Exception as e:
            print("Error", datetime.utcnow(),e)
            raise e


def run_control(groups, queue):
    loop = asyncio.new_event_loop()
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    control = ControlWindow(queue=queue, groups=groups)
    control.show()
    sys.exit(app.exec())

def run_gui(names, queue):
    try:
        stations = {
            "BASE": Station("BASE", "MAIN", 0)
        }
        # DBusQtMainLoop(set_as_default=True)
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        screen = app.primaryScreen()
        geom = screen.availableGeometry()
        asyncio.set_event_loop(loop)
        gui = MainWindow(queue=queue, stations=stations, geom=geom, names=names)
        #gui.showMaximized()
        sys.exit(app.exec())
    except Exception as e:
        print("Error RUN GUI", datetime.utcnow(),e)
        raise e


def generate_groups(
        dataset: Dict[str,Any],
        elements: int) -> Dict[str, str]:
    n = max(math.ceil(len(dataset) / elements),1)
    groups = np.array_split(list(dataset), n)
    return {str(i):g for i,g in enumerate(groups) }

def create_reader(dbdata, queue_set, api_url):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    db_reader = WorkerReader(dbdata, queue_set, api_url)
    db_reader.task()


def cancel(tasks):
    for t in tasks:
        t.cancel()
        t.result()


def generate_queue_set(mp_queue, manager, groups):
    queue_set =  QueueSet(mp_queue, groups)
    for g in groups:
        m_queue = manager.Queue()
        queue_set.add(g, m_queue)
    queue_set.distribute()
    return queue_set

from pathlib import Path
app = typer.Typer()

@app.command("gnss_charts")
def run(settings:Path):#=typer.Argument("settings.yaml",help="Path to settings file")):
    if not isinstance(settings,Path):
        print("SETTINGS", settings.__dict__, type(settings))
        settings  = Path(settings)
    conf = settings.read_text()
    conf_data = yaml.safe_load(conf)
    print(conf_data)
    workers = 6
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # existe mmail loop
        manager = Manager()
        mp_queue = manager.Queue()
        dataset = WorkerReader.load_stations(mp_queue, conf_data["source"]["url"])
        groups = generate_groups(dataset, elements=40)
        # crear queue_set
        queue_set = generate_queue_set(mp_queue, manager, groups)
        # change to queue_set
        # on load stations, take queue set to create by group

        tasks = []
        
        dbopts = conf_data.get("dbdata",{})

        if dbopts:
            dbdata = DBData(**dbopts)
        else:
            print("No db data on settings.yaml")

        task = loop.run_in_executor(
            executor,
            functools.partial(
                create_reader,
                dbdata,
                queue_set, 
                conf_data["source"]["url"]))
        tasks.append(task)


        task = loop.run_in_executor(
            executor,
            partial(run_control, groups, mp_queue))
        tasks.append(task)

        for g, names in groups.items():
            queue  = queue_set.get(g)
            task = loop.run_in_executor(
                executor,
                partial(run_gui, names, queue))
            tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            cancel(tasks)
            raise e
        except KeyboardInterrupt as ki:
            cancel(tasks)
            raise ki

def main():
    app()

if __name__ == "__main__":
    app()
