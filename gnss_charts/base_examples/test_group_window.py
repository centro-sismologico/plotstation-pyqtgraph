import sys
import asyncio
import concurrent.futures
from functools import partial
from qasync import QEventLoop, QThreadExecutor
from PySide6.QtGui import QStandardItemModel, QStandardItem
from PySide6.QtGui import QStaticText
from PySide6.QtCore import QObject
from typing import Dict, List
from multiprocessing import Manager, Queue
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QTreeView
)

class QTreeModel(QStandardItemModel):
    def __init__(self,groups:Dict[str,List[str]]):
        super().__init__()
        self.updateGroups(groups)

    def updateGroups(self, groups):
        for g, itemset  in groups.items():
            item = QStandardItem(g)
            elem = self.appendRow(item)
            for e in itemset:
                item.appendRow(QStandardItem(e))

class MainWindow(QMainWindow):
    def __init__(self, model):
        super().__init__()
        # add qtreeview widget
        widget  = QTreeView()
        widget.setModel(model)
        print(widget)
        self.setCentralWidget(widget)
        self.show()

AEventLoop = type(asyncio.get_event_loop())



class QEventLoopPlus(QEventLoop, AEventLoop):
    pass

def run_gui(groups):
    print("run gui start")
    model  = QTreeModel(groups)

    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    print("Creating gui->")
    gui = MainWindow(model)
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())


if __name__ == "__main__":
    
    groups = {
        "norte":["ATJN","PFRJ","CPCO"],
        "centro":["CCSN","VALN","QSCO"],
        "sur":["LVLL","CONS","PARC"]
    }
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        task = loop.run_in_executor(
            executor,
            partial(run_gui, groups))
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyboardInterrupt as ke:
            raise ke
