from dataclasses import dataclass
from datetime import datetime, timedelta
import pyqtgraph as pg
import pytz

def utc_aware(dt):
    aware_utc_dt = dt.replace(tzinfo=pytz.utc)
    return aware_utc_dt

def ts_utc_aware(ts):
    utc_ts = datetime.utcfromtimestamp(ts)
    aware_utc_dt = utc_ts.replace(tzinfo=pytz.utc)
    return aware_utc_dt


@dataclass
class PlotStation:
    """Esta clase permite gestionar las graficas para 1 estacion"""
    seconds: int # seconds 
    p1: pg.PlotWidget
    p2: pg.PlotWidget
    p3: pg.PlotWidget
    layout: pg.GraphicsLayoutWidget
    curve_width:int = 2

    @property
    def map_axis(self):
        return {"N":"axis1","E": "axis2", "U": "axis3"}

    def pos(self):
        return {
            "p1":self.p1.pos(), 
            "p2":self.p2.pos(), 
            "p3":self.p3.pos()
        }
        
    def __post_init__(self):
        self.p1.showGrid(x=True, y=True)
        self.p2.showGrid(x=True, y=True)
        self.p3.showGrid(x=True, y=True)
        
        self.axis1 = self.p1.plot(
            pen=pg.mkPen("red", width=self.curve_width))
        self.axis2 = self.p2.plot(
            pen=pg.mkPen("blue", width=self.curve_width))
        self.axis3 = self.p3.plot(
            pen=pg.mkPen("green", width=self.curve_width))
       
        
    def update_x_axis(self, now):
        t0 = utc_aware((now+timedelta(seconds=-self.seconds)))
        now = utc_aware(now)
        self.p1.setXRange(t0.timestamp(), now.timestamp())
        self.p2.setXRange(t0.timestamp(), now.timestamp())
        self.p3.setXRange(t0.timestamp(), now.timestamp())
        
    def setData(self, data):
        for name, axis in self.map_axis.items():
            data_axis = data[name]
            curve = getattr(self, axis)
            if curve:
                timelist = [a for a,b  in data_axis]
                valuelist = [b for a,b  in data_axis]
                curve.setData(
                    x=timelist,
                    y=valuelist)
        self.p1.setAutoVisible()
        self.p2.setAutoVisible()
        self.p3.setAutoVisible()


    
    def enableAutoRange(self, axis, auto=False):
        self.p1.enableAutoRange("xy", auto)
        self.p2.enableAutoRange("xy", auto)
        self.p3.enableAutoRange("xy", auto)

    def update_seconds(self, n:int):
        self.seconds = n

