import sys
import asyncio
from typing import Dict
from data import Station
from multi_station_layout_widget import MultiStationLayoutWidget

import pyqtgraph as pg
import functools
import concurrent.futures
import multiprocessing as mp
from multiprocessing import Manager, Queue

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot
)
from qasync import QEventLoop, QThreadExecutor

# test imports
from datetime import datetime
from station_layout_widget import StationLayoutWidget
from station_ring_buffer import RingBuffer as RingBufferTest 
from utils import TimeAxisItem, timestamp
from functools import partial
from plot_station import PlotStation
import random


from join_rb_plot import JoinRingBufferPlot

AEventLoop = type(asyncio.get_event_loop())



class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


class SimpleLayoutWidget(pg.GraphicsLayoutWidget):
    joins = []
    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()
        for join in self.joins:
            join.update()
        #return super().get_frame()

    def add_join(self, joinrb):
        self.joins.append(joinrb) 


def create_ring_buffer(seconds):
    data = dict(
        seconds=seconds,
        name="test",
        time_field="dt_gen",
        ringbuffer=[],
        keys=["N","E","U"]
    )
    rb = RingBufferTest(**data)
    return rb


class MainWindow(QMainWindow):
    groups:Dict[str, MultiStationLayoutWidget]
    stations:Dict[str, Station]

    def __init__(self, groups=[], stations={}):
        super().__init__()
        if not groups:
            groups.append("MAIN")
        self.stations = stations
        parent_layout = QVBoxLayout()
        w = QWidget()
        main_layout = SimpleLayoutWidget(size=(500,100))
        self.jrbp = self.create_station_element(main_layout)
        #win.start(60)
        #self.main_win = self.create_charts(w, groups)
        self.main_win = main_layout
        parent_layout.addWidget(self.main_win)
        w.setLayout(parent_layout)
        self.setCentralWidget(w)
        # the timer could activate on the active window?
        self.set_timer(self.main_win)
        self.show()

    def create_station_element(self, main_layout):
        name = "STAT"
        position = 0
        print("Creating StationLayoutWidget")
        station_layout = StationLayoutWidget()
        plot_station = station_layout.create_chart()#create_chart(pg, win)
        ring_buffer = create_ring_buffer(plot_station.seconds)
        jrbp = JoinRingBufferPlot(
            name,
            position,
            ring_buffer,
            station_layout)
        main_layout.add_join(jrbp)
        main_layout.addItem(station_layout, row=position)
        #jrbp.create_chart()
        return jrbp


    def create_charts(self, parent, groups):
        """
        TODO: Check if parent is for all...
        """
        print("Creating charts")
        for g in groups:
            print(g,self.stations)
            stations_group = {code:station for code, station in
                              self.stations.items() if station.group==g}
            win = MultiStationLayoutWidget(size=(500,100)).start(
                g,
                stations_group)
            print(win)
            self.groups[g] = win
        main_win = self.groups[groups[0]]
        pg.setConfigOptions(antialias=True)
        return main_win

   
    def set_timer(self, win):
        self.timer= QTimer(self)
        self.timer.timeout.connect(win.get_frame)
        self.timer.start(100)


def run_gui():
    stations = {
        "BASE": Station("BASE", "MAIN", 0)
    }
    # DBusQtMainLoop(set_as_default=True)
    print("Init RUNGUI")
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    gui = MainWindow(stations=stations)
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())


if __name__ == "__main__":
    print("Running GUI")
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        task = loop.run_in_executor(
            executor,
            run_gui)
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyboardInterrupt:
            raise KeyboardInterruptError()
