# -*- coding: utf-8 -*-

from dataclasses import dataclass
from datetime import datetime, timedelta
import random
import numpy as np
from networktools.geo import DataItem
from typing import List
from .ring_buffer import StationRingBuffer
from .data import DataTest, Value

@dataclass
class RingBufferTest(StationRingBuffer):
    def update(self, frec=1):
        """
        Para test, esta función debería rescatar de algún lado datos de un stream o cola.
        Para un conjunto N de estaciones, el argumento debería ser un map a los ring_buffers
        """
        now = datetime.utcnow()
        self.refresh()
        #dataN =  [random.uniform(-100,100) for _ in range(10)]        
        #dataE =  np.sin([random.uniform(-100,100) for _ in range(100)])    
        #dataU =  np.cos([random.uniform(-100,100) for _ in range(100)])    
        secs = 2
        dts =  [now + timedelta(seconds=random.uniform(0,200)) for _ in range(secs)] 
        dataE = np.sin([random.uniform(-100,100)*frec for _d in dts]) * np.sin([datetime.timestamp(d)*frec/2 for d in dts])
        dataU = np.cos([datetime.timestamp(d)*frec for d in dts])
        dataN = np.sin([datetime.timestamp(d)*frec/100 for d in dts])**2
        length = []
        for elemN,elemE,elemU,dt in zip(dataN,dataE,dataU,dts):
            index, last = self.add(DataTest(Value(elemN,.01),Value(elemE,.01),Value(elemU,.01),dt)) 
            length.append(self.length)
        return self.getdata()



from networktools.geo import GeoData, TimeData, NEUData, DataPoint, DataItem

@dataclass
class StandardRingBuffer(StationRingBuffer):
    def update(self, frec=1):
        """
        Para test, esta función debería rescatar de algún lado datos de un stream o cola.
        Para un conjunto N de estaciones, el argumento debería ser un map a los ring_buffers
        """
        self.refresh()
        # convert data -> datItem
        data = self.getdata()
        return data



RingBuffer = RingBufferTest


