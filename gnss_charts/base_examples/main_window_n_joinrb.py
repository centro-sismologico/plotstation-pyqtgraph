import sys
import asyncio
from typing import Dict
from data import Station
from multi_station_layout_widget import MultiStationLayoutWidget

import pyqtgraph as pg
import functools
import concurrent.futures
import multiprocessing as mp
from multiprocessing import Manager, Queue

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QListWidget
)
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot
)
from PySide6.QtGui import QFont,QGuiApplication

from qasync import QEventLoop, QThreadExecutor

# test imports
from datetime import datetime
from station_layout_widget import StationLayoutWidget
from station_ring_buffer import RingBuffer as RingBufferTest 
from utils import TimeAxisItem, timestamp
from functools import partial
from plot_station import PlotStation
import random
from join_rb_plot import JoinRingBufferPlot
from typing import List
from control_group import ControlWindow

AEventLoop = type(asyncio.get_event_loop())



class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


def create_ring_buffer(seconds):
    data = dict(
        seconds=seconds,
        name="test",
        time_field="dt_gen",
        ringbuffer=[],
        keys=["N","E","U"]
    )
    rb = RingBufferTest(**data)
    return rb



class SimpleMultiplotLayoutWidget(pg.GraphicsLayoutWidget):
    joins:List[JoinRingBufferPlot] = []
    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()
        for join in self.joins:
            join.update()

    # def set_update(self, update):
    #     self.update = update

    def add_join(self, joinrb):
        self.joins.append(joinrb) 



class MainWindow(QMainWindow):
    groups:Dict[str, MultiStationLayoutWidget]
    stations:Dict[str, Station]

    def __init__(self, groups=[], stations={}, geom=None):
        super().__init__()
        if not groups:
            groups.append("MAIN")
        self.stations = stations
        parent_layout = QVBoxLayout()
        w = QWidget()
        main_layout = SimpleMultiplotLayoutWidget(size=(500,100))
        #main_layout.useOpenGL()
        self.jrbp = self.create_station_element(main_layout)
        #win.start(60)
        #self.main_win = self.create_charts(w, groups)
        self.main_win = main_layout
        parent_layout.addWidget(self.main_win)
        w.setLayout(parent_layout)
        self.setCentralWidget(w)
        # the timer could activate on the active window?
        self.set_timer(self.main_win)
        # neew activate with a button
        self.showMaximized()
        self.control_window(self.jrbp)

    def control_window(self, groups):
        print("Loading groups->", groups)
        control = ControlWindow(self, groups)
        control.show()

    def create_station_element(self, main_layout):
        names = [f"STA{i:02d}" for i in range(45)]
        jrbp_dict = {}
        plot_before = None
        for position, name in enumerate(names):
            station_layout = StationLayoutWidget()
            station_layout.setContentsMargins(0, 0, 0, 0)
            title_names = False
            axis_time = False
            if position == 0:
                title_names =  True
            elif position == len(names)-1:
                axis_time = True
            plot_station = station_layout.create_chart(
                name,
                title_names, 
                axis_time, 
                main_layout,
                plot_before)#create_chart(pg, win)
            # adding item to main layout
            plot_before = plot_station
            main_layout.addItem(station_layout)
            # lalala
            ring_buffer = create_ring_buffer(plot_station.seconds)
            jrbp = JoinRingBufferPlot(
                name, 
                position, 
                ring_buffer,
                station_layout)
            jrbp_dict[name] = jrbp
            main_layout.add_join(jrbp)
            main_layout.addItem(station_layout, row=position, col=0)
        #jrbp.create_chart()
        return jrbp_dict


    def create_charts(self, parent, groups):
        """
        TODO: Check if parent is for all...
        """
        print("Creating charts")
        for g in groups:
            stations_group = {code:station for code, station in
                              self.stations.items() if station.group==g}
            win = MultiStationLayoutWidget(size=(500,100)).start(
                g,
                stations_group)
            print(win)
            self.groups[g] = win
        main_win = self.groups[groups[0]]
        pg.setConfigOptions(antialias=True)
        return main_win

   
    def set_timer(self, win):
        self.timer= QTimer(self)
        self.timer.timeout.connect(win.get_frame)
        self.timer.start(500)


def run_gui():
    stations = {
        "BASE": Station("BASE", "MAIN", 0)
    }
    # DBusQtMainLoop(set_as_default=True)
    print("Init RUNGUI")
    app = QApplication(sys.argv)
    screen = app.primaryScreen()
    #print("Screens", screen, screen.name())
    geom = screen.availableGeometry()
    #print("Geom screen", geom)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    gui = MainWindow(stations=stations, geom=geom)
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())


if __name__ == "__main__":
    print("Running GUI")
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        task = loop.run_in_executor(
            executor,
            run_gui)
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyboardInterrupt as ki:
            raise ki
