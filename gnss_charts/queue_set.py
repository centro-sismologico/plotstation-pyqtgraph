  
from dataclasses import dataclass
import queue as q
import asyncio
import multiprocessing as mp
from typing import Union, List, Dict, Type, Any, Optional,get_args
from asyncio import wait_for, shield
import inspect
from pydantic import PositiveFloat, ValidationError, validator
from rich import print
from functools import cached_property

mpQueue = type(mp.Queue())

QueueType = Union[q.Queue, mpQueue]

@dataclass
class QueuePair:
    name: str
    queue: Optional[QueueType]
    
    class Config:
        arbitrary_types_allowed = True

    def qsize(self):
        return self.queue.qsize()

    @validator("queue",pre=True)
    def queue_must_be_some(cls, v:QueueType) -> QueueType:
         if not isinstance(v,get_args(QueueType)):
             raise ValueError("value is not type Queue")
         return v

    def get(self):
        return self.read()

    def put(self, value):
        return self.send(value)

    def task_done(self):
        self.queue.task_done()

    @property
    def to(self):
        return self.timeout if self.timeout>0 else None


    def send(self, value:Any):
        if self.queue:
            self.queue.put(value)


    def read(self) -> List[Any]:
        values = []
        if self.queue:
            if self.queue.qsize() > 0:
                for _ in range(self.queue.qsize()):
                    value = self.queue.get()
                    if value:
                        values.append(value)
                self.queue.task_done()
            return values
        return []


    def unprotect(self):
        self.protected = False

    def set_timeout(self, n:PositiveFloat):
        self.timeout = n

from dataclasses import field

@dataclass
class QueueSet:
    source_queue: mpQueue
    groups:Dict[str,str]
    queues:Optional[Dict[str, QueuePair]] = field(default_factory=dict)


    def distribute(self):
        """
        Once read the source send by queue paired (code, metadata)
        Then resend by group queue after created QueueSet instance
        """
        if self.source_queue.qsize() > 0:
            for _ in range(self.source_queue.qsize()):
                data = self.source_queue.get()
                self.put(data)
                self.source_queue.task_done()

    @cached_property
    def reverse_groups(self):
        reverse = {}
        for k,v in self.groups.items():
            for name in v:
                reverse[name] = k
        return reverse

    def add(self, name:str, queue:QueueType):
        queue = QueuePair(name, queue)
        if isinstance(self.queues, dict):
            self.queues[name] = queue

    def get(self, name:str):
        if self.queues:
            return self.queues.get(name)
        
    def send(self, key, value):
        group = self.group(key)
        if group:
            self.get(group).send(value)

    def put(self, data):
        if isinstance(data.data, dict):
            key= data.data.get("code")
        else:
            key= data.data.data.station
        value = data
        self.send(key, value)

    def read(self, key)->Any:
        group = self.group(key)
        return self.get(group).read()

    def set_timeout(self, n:PositiveFloat):
        if self.queues:
            for queue in self.queues.values():
                queue.set_timeout(n)
        
    def unprotect(self):
        for queue in self.queues.values():
            queue.unprotect()

    def group(self, name):
        return self.reverse_groups.get(name)

