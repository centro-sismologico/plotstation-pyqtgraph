from dataclasses import dataclass
from datetime import datetime, timedelta
from .station_ring_buffer import RingBuffer as StationRingBuffer
from .station_layout_widget import StationLayoutWidget

@dataclass
class JoinRingBufferPlot:
    name: str
    position: int
    ring_buffer: StationRingBuffer
    plot:  StationLayoutWidget
    width: int = 1000
    height: int = 150
  
    def __post_init__(self):
        self.plot.set_update(self.update)

    def update_seconds(self, n:int):
        self.plot.update_seconds(n)

    def create_chart(self):
        self.plot.create_chart(self.name, True, True)

    def get_frame(self):
        self.plot.get_frame()

    def update(self):
        """
        Axis and data, are the name and the new data from ringbuffer
        """
        # update take from ring buffer
        data = self.ring_buffer.update(self.position+1)
        # iterate over data
        #put over the plot
        now = datetime.utcnow()
        self.plot.setData(data)    
        self.plot.update_x_axis(now)
        self.plot.updateViewBox()

    @property
    def limits(self):
        return self.rinring_buffer.limits



@dataclass
class ItemDataJoinRingBufferPlot:
    name: str
    position: int
    ring_buffer: StationRingBuffer
    plot:  StationLayoutWidget
    width: int = 1000
    extra_height: int = 10
  
    def __post_init__(self):
        self.plot.set_update(self.update)

    def update_seconds(self, n:int):
        self.plot.update_seconds(n)

    def create_chart(self):
        self.plot.create_chart(self.name, True, True)

    def get_frame(self):
        self.plot.get_frame()

    def update(self):
        """
        Axis and data, are the name and the new data from ringbuffer
        """
        # update take from ring buffer
        data = self.ring_buffer.update(self.position+1)
        # iterate over data
        #put over the plot
        now = datetime.utcnow()
        self.plot.updateViewBox(self.extra_height)
        self.plot.setData(data)    
        self.plot.update_x_axis(now)


    def add(self, item):
        self.ring_buffer.add(item)

    @property
    def limits(self):
        return self.rinring_buffer.limits

