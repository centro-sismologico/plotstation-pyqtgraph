from dataclasses import dataclass
from datetime import datetime
from networktools.geo import DataItem

@dataclass
class Value:
    value:float
    error:float=0.01

@dataclass
class DataTest(DataItem):
    N:Value
    E:Value
    U:Value
    dt_gen: datetime

    def get_datetime(self):
        return self.dt_gen

@dataclass
class Station:
    code: str 
    group: str
    position: int

    def generate(self):
        pass

    def obtain(self):
        pass
    
