from typing import Callable
import pyqtgraph as pg
from pyqtgraph.graphicsItems.GraphicsLayout import GraphicsLayout
from PySide6.QtGui import QFont,QGuiApplication
from typing import Optional
from PySide6.QtCore import QRectF, QPoint
from .utils import TimeAxisItem
from .plot_station import PlotStation
 
class StationLayoutWidget(GraphicsLayout):
    """
    Maybe inherit with PlotStation ::
    self.plots -> self
    """
    plots = None
    seconds:int = 30
    plot_station:Optional[PlotStation] = None
    linked_plot:Optional[PlotStation] = None
    elements:int = 45

    # def __init__(self, parent=None, border=None, *args, **kwargs):
    #     super().__init__(parent)
    #     # need to set anme, adnd conf
    #     self.create_chart()

    def set_time(self, seconds):
        self.seconds = seconds

    def start(self, name:str, seconds:int = 30):
        self.seconds = seconds
        self.name = name
        self.create_chart(self.name, True, True, None)

    def update_x_axis(self, now):
        if self.plots:
            self.plots.update_x_axis(now)

    def setData(self, data):
        if self.plots:
            self.plots.setData(data)

    def get_frame(self):
        # rather than eating up cpu cycles by perpetually updating "Updating plot",
        # we will only update it opportunistically on a redraw.
        # self.request_draw()
        if self.update:
            self.update()
        #return super().get_frame()

    @property
    def map_axis(self):
        if self.plots:
            return self.plots.map_axis

    def update_seconds(self, n:int):
        if self.plots:
            self.plots.update_seconds(n)
    
    def set_update(self, update:Callable):
        self.update = update

    def set_name(self, name, angle=0, **conf):
        self.label = self.addLabel(name, angle, **conf)

    def set_plots(self, plot_station):
        self.plots = plot_station

    def create_chart(self, 
                     name:str, 
                     title_names:bool, 
                     axis_time:bool, 
                     parent_layout:None,
                     plot_before:Optional[PlotStation]=None):
        """
        Create a chart layout
        """
        self.parent = parent_layout
        # Enable antialiasing for prettier plots
        label_text = name
        conf = {
            "width":"1.5", 
            "color":"orange", 
            "bold": True, 
            "size": "16",  "col":0}
        self.set_name(label_text, **conf)
        axis_names = ["N","E","U"]
        axis = {n:TimeAxisItem(orientation='bottom',
                               showValues=axis_time) for n in
                axis_names}

        if not axis_time:
            for n, axe in axis.items():
                axe.setHeight(h=2)
                axe.setLabel(text="", units=0)
                axe.setStyle(showValues=False)
        else:
            font=QFont()
            font.setPixelSize(16)
            font.setBold(True)
            for n, axe in axis.items():
                axe.setLabel(text="Tiempo Local")
                axe.setTickFont(font)

        if title_names:
            font=QFont()
            font.setPixelSize(6)
            font.setBold(True)
            p1 = self.addPlot(
                title="N",
                axisItems={
                    'bottom':axis["N"]}, col=1)
            p2 = self.addPlot(
                title="E",
                axisItems={
                    'bottom':axis["E"]}, col=2)
            p3 = self.addPlot(
                title="U",
                axisItems={
                    'bottom': axis["U"]}, col=3)

            p1.titleLabel.item.setFont(font)
            p2.titleLabel.item.setFont(font)
            p3.titleLabel.item.setFont(font)
            p1.titleLabel.item.adjustSize()
            p2.titleLabel.item.adjustSize()
            p3.titleLabel.item.adjustSize()

        else:
            p1 = self.addPlot(
                axisItems={
                    'bottom': axis["N"] }, col=1)
            p2 = self.addPlot(
                axisItems={
                    'bottom': axis["E"]
                }, col=2)
            p3 = self.addPlot(
                axisItems={
                    'bottom':axis["U"]
                }, col=3)


        if plot_before:
            pb = plot_before
            self.linked_plot = pb
            # p1.getAxis("bottom").linkToView(pb.p1.vb)
            # p2.getAxis("bottom").linkToView(pb.p2.vb)
            # p3.getAxis("bottom").linkToView(pb.p3.vb)
        plot_station = PlotStation(
            self.seconds,
            p1, p2, p3, parent_layout)
        plot_station.enableAutoRange('xy', True)  ## stop auto-scaling
        ## after the first data set is plotted
        self.set_plots(plot_station)
        self.plot_station = plot_station
        return plot_station

    def set_elements(self, n):
        self.elements = n

    def updateViewBox(self, extra_height:int=0):
        """
        TODO: More control to adjust charts
        """
        rect = self.parent.viewRect()
        height_top_bottom = 50 
        height = ((rect.height()-height_top_bottom)/self.elements)*.9 + extra_height

        label_rect = self.label.boundingRect()
        w_available = int(.9*((rect.width() - label_rect.width())/3))

        self.layout.setColumnFixedWidth(1,w_available*1.05)
        self.layout.setColumnFixedWidth(2,w_available*1.05)
        self.layout.setColumnFixedWidth(3,w_available*1.05)


        qrect =  QRectF(QPoint(0,0),QPoint(w_available,height))


        # self.plot_station.p1\
        #     .getAxis("bottom")\
        #     .setWidth(w_available)
        # self.plot_station.p1.vb.setXRange(0,w_available)

        p1_item = self.plot_station.p1#.getPlotItem()
        p1_item.setXRange(0,w_available)
        p1_y_axis = p1_item.getAxis("left")

        # self.plot_station.p2\
        #     .getAxis("bottom")\
        #     .setWidth(w_available)
        # self.plot_station.p2.vb.setXRange(0,w_available)

        p2_item = self.plot_station.p2#.getPlotItem()
        p2_item.setXRange(0,w_available)
        p2_y_axis = p2_item.getAxis("left")

        # self.plot_station.p3\
        #     .getAxis("bottom")\
        #     .setWidth(w_available)
        # self.plot_station.p3.vb.setXRange(0,w_available)

        p3_item = self.plot_station.p3#.getPlotItem()
        p3_item.setXRange(0,w_available)
        p3_y_axis = p3_item.getAxis("left")
        
        font = QFont()
        font.setPixelSize(8)
        p1_y_axis.setStyle(tickFont = font, tickLength = 10)
        p2_y_axis.setStyle(tickFont = font, tickLength = 10)
        p3_y_axis.setStyle(tickFont = font, tickLength = 10)

        p1_item.setLimits(minYRange=-5, maxYRange=5)
        p2_item.setLimits(minYRange=-5, maxYRange=5)
        p3_item.setLimits(minYRange=-5, maxYRange=5)


    def updateViews(self):
        pb = self.linked_plot
        p = self.plot_station
        p.p1.setGeometry(pb.p1.vb.sceneBoundingRect())
        p.p1.linkedViewChanged(pb.p1.vb, p.p1.XAxis)

        p.p2.setGeometry(pb.p2.vb.sceneBoundingRect())
        p.p2.linkedViewChanged(pb.p2.vb, p.p2.XAxis)

        p.p3.setGeometry(pb.p3.vb.sceneBoundingRect())
        p.p3.linkedViewChanged(pb.p3.vb, p.p3.XAxis)
